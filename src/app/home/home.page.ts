import { Component } from '@angular/core';
import {NavController} from '@ionic/angular';
import {NativeAudio} from '@ionic-native/native-audio/ngx';
import {BackgroundMode} from '@ionic-native/background-mode/ngx';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  constructor(public navCtrl: NavController,
              public nativeAudio: NativeAudio ,
              public backgroundMode: BackgroundMode) {
    this.nativeAudio.preloadSimple('audio1', 'assets/audio1.mp3').then((msg) => {
      console.log('message: ' + msg);
    }, (error) => {
      console.log('error: ' + error);
    });
  }

  public playAudio(){
    this.backgroundMode.enable();
    this.backgroundMode.on('activate').subscribe(() => {
      this.nativeAudio.play('audio1');
    });
    this.nativeAudio.play('audio1', () => console.log('audio1 is done playing'));
  }

}
